import {Router} from 'express';
import {resolve} from 'path';
import {ObjectID} from 'mongodb';

import {getDb} from '../mongo';

const router = Router();

// home route HTML return
router.get('/', (req, res) => {
  res.sendFile(resolve('public/html/home.html'));
});

router.get('/game', (req, res) => {
  res.sendFile(resolve('public/html/game.html'));
});

router.get('/finish', (req, res) => {
  res.sendFile(resolve('public/html/finish.html'));
});

router.get('/questionnaire', (req, res) => {
  res.sendFile(resolve('public/html/questionnaire.html'));
});

router.get('/ux', (req, res) => {
  res.sendFile(resolve('public/html/ux.html'));
});

router.get('/game/:id', (req, res) => {
  const db = getDb();

  let oid;

  try {
    oid = ObjectID(req.params.id) || null;
  } catch (err) {
    return res.redirect('/game');
  }

  db.collection('polls').find({_id: oid}).toArray((err, data) => {
    if (err) {
      return res.status(500).end();
    }

    if (data[0]) {
      if (data[0].polls.length) {
        return res.redirect(`../finish/${id}`)
      } else {
        return res.sendFile(resolve('public/html/game.html'));
      }
    } else {
      return res.redirect('/');
    }
  });
});

router.get('/finish/:id', (req, res) => {
  const db = getDb();

  let oid;

  try {
    oid = ObjectID(req.params.id) || null;
  } catch (err) {
    return res.redirect('/game');
  }

  db.collection('polls').find({_id: oid}).toArray((err, data) => {
    if (err) {
      return res.status(500).end();
    }

    if (data[0]) {
      return res.sendFile(resolve('public/html/finish.html'));
    } else {
      return res.redirect('/finish');
    }
  });
});

// api routes

router.post('/user', (req, res) => {
  const db = getDb();
  const collection = {
    gamePolls: [],
    questionnairePolls: [],
    user: req.body,
    gameExperience: [],
    questionnaireExperience: [],
    timeInGame: -1,
    isMobile: false,
    gameFirst: false,
    date: new Date()
  };
  //const collection = { user: req.body, timeInGame: -1 };

  db.collection('polls').insertOne(collection).then((data) => {
    res.status(200).json(data.insertedId);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.post('/comment/:id', (req, res) => {
  const db = getDb();

  const _id = ObjectID(req.params.id);

  db.collection('polls').findOneAndUpdate({_id},
    {
      $set: {
        comment: req.body.text,
      }
    }, err => {
      if (err) {
        return res.status(500).json(err);
      }
      res.status(201).end();
    });

});

router.post('/save/polls/game/:id', (req, res) => {
  const db = getDb();

  const _id = ObjectID(req.params.id);
  db.collection('polls').findOneAndUpdate({_id},
    {
      $set: {
        timeInGame: req.body.timeInGame,
        gameFirst: req.body.gameFirst,
        isMobile: req.body.isMobile,
        gamePolls: req.body.polls
      }
    }, err => {
      if (err) {
        return res.status(500).json(err);
      }
      res.status(201).end();
    });

});

router.post('/save/polls/questionnaire/:id', (req, res) => {
  const db = getDb();

  const _id = ObjectID(req.params.id);
  db.collection('polls').findOneAndUpdate({_id},
    {$set: {questionnairePolls: req.body.questionnaire}}, err => {
      if (err) {
        return res.status(500).json(err);
      }
      res.status(201).end();
    });

});

router.post('/save/ux/game/:id', (req, res) => {
  const db = getDb();

  const _id = ObjectID(req.params.id);

  db.collection('polls').findOneAndUpdate({_id},
    {$set: {gameExperience: req.body.experience}}, err => {
      if (err) {
        return res.status(500).json(err);
      }
      res.status(201).end();
    });
});

router.post('/save/ux/questionnaire/:id', (req, res) => {
  const db = getDb();

  const _id = ObjectID(req.params.id);

  db.collection('polls').findOneAndUpdate({_id},
    {$set: {questionnaireExperience: req.body.experience}}, err => {
      if (err) {
        return res.status(500).json(err);
      }
      res.status(201).end();
    });
});

router.get('/results', (req, res) => {
  const db = getDb();

  db.collection('polls').find({timeInGame: {$gt: 0}}).sort({timeInGame: 1}).limit(6).toArray((err, data) => {
    if (err) {
      res.status(500).json(err);
      return;
    }

    return res.status(200).json(data);
  });
});

router.get('/results/:id', (req, res) => {
  const db = getDb();

  let _id;

  try {
    _id = ObjectID(req.params.id);
  } catch (err) {
    return res.status(400).end();
  }

  db.collection('polls').find({_id}).toArray((err, user) => {
    if (err) {
      return res.status(500).end();
    }

    if (user[0]) {
      return res.status(200).json(user[0].timeInGame);
    } else {
      return res.status(404).end()
    }
  })
});

router.post('/ux/questions-en', (req, res) => {
  const db = getDb();

  const leftValues = ['annoying', 'not understandable', 'creative', 'easy to learn', 'valuable', 'boring',
    'not interesting', 'unpredictable', 'fast', 'inventive', 'obstructive', 'good', 'complicated', 'unlikable',
    'usual', 'unpleasant', 'secure', 'motivating', 'meets expectations', 'inefficient',
    'clear', 'impractical', 'organized', 'attractive', 'friendly', 'conservative'];
  const rightValues = ['enjoyable', 'understandable', 'dull', 'difficult to learn', 'inferior', 'exciting',
    'interesting', 'predictable', 'slow', 'conventional', 'supportive', 'bad', 'easy', 'pleasing',
    'leading edge', 'pleasant', 'not secure', 'demotivating', 'does not meet expectations', 'efficient',
    'confusing', 'practical', 'cluttered', 'unattractive', 'unfriendly', 'innovative'];

  let questions = [];

  for (let i = 0; i < leftValues.length; i++) {
    questions.push({id: i + 1, leftValue: leftValues[i], rightValue: rightValues[i]});
  }

  db.collection('uxEN').remove({}).then(() => {
    db.collection('uxEN').insertMany(questions).then((data) => {
      res.status(200).json(questions);
    }).catch(err => {
      res.status(500).json(err);
    });
  });
});

router.post('/ux/questions-hr', (req, res) => {
  const db = getDb();

  const leftValues = ['iritirajuće', 'nerazumljivo', 'kreativno', 'lagano za naučiti', 'vrijedno', 'dosadno',
    'nezanimljivo', 'nepredvidivo', 'brzo', 'inovativno', 'ometajuće', 'dobro', 'složeno', 'nedopadljivo',
    'uobičajeno', 'neugodno', 'sigurno', 'motivirajuće', 'zadovoljava očekivanja', 'neefikasno',
    'jasno', 'nepraktično', 'organizirano', 'privlačno', 'prijateljski', 'konzervativno'];
  const rightValues = ['ugodno', 'razumljivo', 'suhoparno', 'teško za naučiti', 'manje vrijedno', 'uzbudljivo',
    'zanimljivo', 'predvidivo', 'sporo', 'obično', 'podržavajuće', 'loše', 'jednostavno', 'udovoljavajuće',
    'novo', 'ugodno', 'nesigurno', 'demotivirajuće', 'ne zadovoljava očekivanja', 'efikasno',
    'zbunjujuće', 'praktično', 'pretrpano', 'neprivlačno', 'neprijateljski', 'inovativno'];

  let questions = [];

  for (let i = 0; i < leftValues.length; i++) {
    questions.push({id: i + 1, leftValue: leftValues[i], rightValue: rightValues[i]});
  }

  db.collection('uxHR').remove({}).then(() => {
    db.collection('uxHR').insertMany(questions).then((data) => {
      res.status(200).json(questions);
    }).catch(err => {
      res.status(500).json(err);
    });
  });
});

router.get('/ux/questions-en', (req, res) => {
  const db = getDb();

  db.collection('uxEN').find({}).toArray((err, q) => {
    if (err) {
      return res.status(500).end();
    }

    if (q) {
      return res.status(200).json(q);
    } else {
      return res.status(404).end()
    }
  })
});

router.get('/ux/questions-hr', (req, res) => {
  const db = getDb();

  db.collection('uxHR').find({}).toArray((err, q) => {
    if (err) {
      return res.status(500).end();
    }

    if (q) {
      return res.status(200).json(q);
    } else {
      return res.status(404).end()
    }
  })
});

router.post('/translation', (req, res) => {
  const db = getDb();

  db.collection('translation').insertMany(req.body).then((data) => {
    res.status(200).json(req.body);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.get('/translation', (req, res) => {
  const db = getDb();

  db.collection('translation').find({}).toArray((err, q) => {
    if (err) {
      return res.status(500).end();
    }

    if (q) {
      return res.status(200).json(q);
    } else {
      return res.status(404).end()
    }
  })
});

export default router;
