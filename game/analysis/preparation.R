rm(list = ls())

library(jsonlite)
library(stringr)

path = 'C:\\WTP\\game\\'


# demographics
podaci <- read.csv(paste(path, 'results.csv', sep = ""))
podaci <- podaci[!(podaci$gamePolls == "[]"), ]
podaci <- podaci[!(podaci$questionnairePolls == "[]"), ]
podaci <- podaci[!(podaci$gameExperience == "[]"), ]
podaci <- podaci[!(podaci$questionnaireExperience == "[]"), ]

podaci$X_id <- gsub('ObjectId\\(', '', podaci$X_id)
podaci$X_id <- gsub(')', '', podaci$X_id)


demografski <-
  data.frame(
    ID = podaci$X_id,
    GENDER = podaci$user.gender,
    COUNTRY = podaci$user.country,
    AGE = podaci$user.age,
    EMPLOYMENT = podaci$user.employment,
    INCOME = podaci$user.income,
    FAMILIARITY = podaci$user.familiarity,
    CAR_CNT = podaci$user.carCnt,
    CAR_OWNER = podaci$user.carOwner,
    EV_CAR_OWNER = podaci$user.evCarOwner,
    LICENCE = podaci$user.licence,
    GAME_FIRST = podaci$gameFirst
  )

# all invalid records including ones marked as inconsistent by UX analysis
invalidIDs = list(
  '5aa78e0d33671002c42b361b',
  '5aab7fe233671002c42b361c',
  '5aab84ca33671002c42b3621',
  '5ab1541e5b2f8315f99a60c1',
  '5ab157685b2f8315f99a60c2',
  '5ab23ed25b2f8315f99a60ca',
  '5aba7193989561145624ee3b',
  '5aba7757989561145624ee3f',
  '5aba7d08989561145624ee42',
  '5abb7d1f989561145624ee53',
  '5abb7e41989561145624ee57',
  '5abb8040989561145624ee58',
  '5abb7d2d989561145624ee54',
  '5abcbd42989561145624ee62',
  '5abcca89989561145624ee64',
  '5abce7a8989561145624ee67',
  '5abfabf9989561145624ee6b',
  '5ad26f24989561145624ee92',
  '5ad27666989561145624ee93',
  '5ad33dfc989561145624eea9',
  '5ad469ef989561145624eebc',
  '5ad4c223989561145624eebf',
  '5ad3937a989561145624eeb3',
  '5ad522c6989561145624eec7',
  '5ad5be51989561145624eecb',
  '5ad74ebd989561145624eeea',
  '5ad730f6989561145624eee9',
  '5ad712de989561145624eee7',
  '5ade2b37989561145624eefc',
  '5ade1fc5989561145624eef9',
  '5ade51a1989561145624ef0e',
  '5ad97748989561145624eef2',
  '5ae24ffb989561145624ef33',
  '5ae27085989561145624ef36',
  '5ae04b3e989561145624ef1d',
  '5adef6ec989561145624ef14',
  '5ae22ce7989561145624ef29',
  '5ae178a4989561145624ef24',
  '5ae348d4989561145624ef3a'
)


# game WTP
podaci$gamePolls <- gsub('\\[', '', podaci$gamePolls)
podaci$gamePolls <- gsub('\\]', '', podaci$gamePolls)

wtp <- data.frame()

ankete = podaci[, c("X_id", "gamePolls")]


for (i in 1:nrow(ankete)) {
  anketa <-
    as.data.frame(str_split_fixed(ankete$gamePolls, "\\}" ,  6))
  for (j in 1:6) {
    wtp <-
      rbind(wtp,
            data.frame(
              ID = ankete[i, "X_id"],
              SURVEY_TYPE = "GAME",
              POLL = anketa[i, paste("V", j, sep = "")]
            ))
  }
  
}

wtp$POLL <- gsub('\\,\\{', '\\{', wtp$POLL)

for (i in 1:nrow(wtp)) {
  if (!endsWith(wtp$POLL[i], '}')) {
    wtp$POLL[i] <- paste(wtp$POLL[i], '}', sep = "")
  }
}
wtp$POLL <- gsub('\\}}', '\\}', wtp$POLL)

game <- data.frame()

n <- 0

for (i in 1:nrow(wtp)) {
  n <- n + 1
  
  poll <- fromJSON(wtp$POLL[i])
  
  if (n == 1) {
    idToCheck <- toString(wtp$ID[i])
    wtpFirst <- poll$wtp
    duplicate <- TRUE
  } else {
    if (wtpFirst != poll$wtp) {
      duplicate <- FALSE
    }
  }
  
  game <-
    rbind(
      game,
      data.frame(
        ID = wtp$ID[i],
        SURVEY_TYPE = "GAME",
        DAYTIME = poll$daytime,
        SOC1 = poll$soc1,
        SOC2 = poll$soc2,
        DELTA = poll$delta,
        RP = poll$cost,
        WTP = poll$wtp
      )
    )
  
  if (n == 6) {
    n <- 0
    
    if (duplicate == TRUE) {
      invalidIDs <- c(invalidIDs, idToCheck)
    }
  }
}

# questionnaire WTP
podaci$questionnairePolls <-
  gsub('\\[', '', podaci$questionnairePolls)
podaci$questionnairePolls <-
  gsub('\\]', '', podaci$questionnairePolls)

wtp <- data.frame()

ankete = podaci[, c("X_id", "questionnairePolls")]


for (i in 1:nrow(ankete)) {
  anketa <-
    as.data.frame(str_split_fixed(ankete$questionnairePolls, "\\}" ,  6))
  for (j in 1:6) {
    wtp <-
      rbind(wtp,
            data.frame(
              ID = ankete[i, "X_id"],
              SURVEY_TYPE = "QUESTIONNAIRE",
              POLL = anketa[i, paste("V", j, sep = "")]
            ))
  }
  
}

wtp$POLL <- gsub('\\,\\{', '\\{', wtp$POLL)

for (i in 1:nrow(wtp)) {
  if (!endsWith(wtp$POLL[i], '}')) {
    wtp$POLL[i] <- paste(wtp$POLL[i], '}', sep = "")
  }
}
wtp$POLL <- gsub('\\}}', '\\}', wtp$POLL)
wtp$POLL <-
  gsub('\\}\\{(.*)', '\\}', wtp$POLL) #doing this because of case when there is 7 saved answers


questionnaire <- data.frame()

n <- 0

for (i in 1:nrow(wtp)) {
  n <- n + 1
  
  poll <- fromJSON(wtp$POLL[i])
  
  if (n == 1) {
    idToCheck <- toString(wtp$ID[i])
    wtpFirst <- poll$wtp
    duplicate <- TRUE
  } else {
    if (wtpFirst != poll$wtp) {
      duplicate <- FALSE
    }
  }
  
  questionnaire <-
    rbind(
      questionnaire,
      data.frame(
        ID = wtp$ID[i],
        SURVEY_TYPE = "QUESTIONNAIRE",
        DAYTIME = poll$daytime,
        SOC1 = poll$soc1,
        SOC2 = poll$soc2,
        DELTA = poll$delta,
        RP = poll$cost,
        WTP = poll$wtp
      )
    )
  
  if (n == 6) {
    n <- 0
    
    if (duplicate == TRUE) {
      invalidIDs <- c(invalidIDs, idToCheck)
    }
  }
}


# game UX
podaci$gameExperience <- gsub('\\[', '', podaci$gameExperience)
podaci$gameExperience <- gsub('\\]', '', podaci$gameExperience)

ankete = podaci[, c("X_id", "gameExperience", "gameFirst")]

ankete <-
  ankete[!(ankete$X_id %in% invalidIDs),] #u slu�aju da se iz UX mi�u invalid

uxGame <- data.frame()
for (i in 1:nrow(ankete)) {
  uxGame <-
    rbind(uxGame, data.frame(
      ID = ankete$X_id[i],
      GAME_FIRST = ankete$gameFirst[i],
      str_split_fixed(ankete$gameExperience[i], "\\}," ,  26)
    ))
}


for (i in 1:26) {
  uxGame[[paste('X', i, sep = '')]] <-
    gsub('"', '', uxGame[[paste('X', i, sep = '')]])
  uxGame[[paste('X', i, sep = '')]] <-
    gsub('\\{', '', uxGame[[paste('X', i, sep = '')]])
  uxGame[[paste('X', i, sep = '')]] <-
    gsub('\\}', '', uxGame[[paste('X', i, sep = '')]])
  str <- ''
  str <- paste('id:', i, sep = '')
  str <- paste(str, ',answer:', sep = '')
  uxGame[[paste('X', i, sep = '')]] <-
    gsub(str, '', uxGame[[paste('X', i, sep = '')]])
}


# questionnaire UX
podaci$questionnaireExperience <-
  gsub('\\[', '', podaci$questionnaireExperience)
podaci$questionnaireExperience <-
  gsub('\\]', '', podaci$questionnaireExperience)

ankete = podaci[, c("X_id", "questionnaireExperience", "gameFirst")]

ankete <-
  ankete[!(ankete$X_id %in% invalidIDs),] #u slu�aju da se iz UX mi�u invalid

uxQuestionnaire <- data.frame()
for (i in 1:nrow(ankete)) {
  uxQuestionnaire <-
    rbind(uxQuestionnaire,
          data.frame(
            ID = ankete$X_id[i],
            GAME_FIRST = ankete$gameFirst[i],
            str_split_fixed(ankete$questionnaireExperience[i], "\\}," ,  26)
          ))
}

for (i in 1:26) {
  uxQuestionnaire[[paste('X', i, sep = '')]] <-
    gsub('"', '', uxQuestionnaire[[paste('X', i, sep = '')]])
  uxQuestionnaire[[paste('X', i, sep = '')]] <-
    gsub('\\{', '', uxQuestionnaire[[paste('X', i, sep = '')]])
  uxQuestionnaire[[paste('X', i, sep = '')]] <-
    gsub('\\}', '', uxQuestionnaire[[paste('X', i, sep = '')]])
  str <- ''
  str <- paste('id:', i, sep = '')
  str <- paste(str, ',answer:', sep = '')
  uxQuestionnaire[[paste('X', i, sep = '')]] <-
    gsub(str, '', uxQuestionnaire[[paste('X', i, sep = '')]])
}


#writing into files after removing more invalid results
for (i in 1:length(invalidIDs)) {
  demografski <- demografski[!(demografski$ID == invalidIDs[[i]]),]
  game <- game[!(game$ID == invalidIDs[[i]]),]
  questionnaire <-
    questionnaire[!(questionnaire$ID == invalidIDs[[i]]),]
}


write.csv(demografski,
          file = "DEMOGRAPHICS.csv",
          row.names = FALSE,
          quote = FALSE)
write.csv(game,
          file = "WTP_game.csv",
          row.names = FALSE,
          quote = FALSE)
write.csv(questionnaire,
          file = "WTP_questionnaire.csv",
          row.names = FALSE,
          quote = FALSE)
write.table(
  uxGame[,-c(2)],
  file = "UX_game.tsv",
  sep = "\t",
  row.names = FALSE,
  col.names = FALSE,
  quote = FALSE
)
write.table(
  uxQuestionnaire[,-c(2)],
  file = "UX_questionnaire.tsv",
  sep = "\t",
  row.names = FALSE,
  col.names = FALSE,
  quote = FALSE
)
write.table(
  uxGame[(uxGame$GAME_FIRST == 'true'),],
  file = "UX_game_gameFirst_TRUE.tsv",
  sep = "\t",
  row.names = FALSE,
  col.names = FALSE,
  quote = FALSE
)
write.table(
  uxGame[(uxGame$GAME_FIRST == 'false'),],
  file = "UX_game_gameFirst_FALSE.tsv",
  sep = "\t",
  row.names = FALSE,
  col.names = FALSE,
  quote = FALSE
)
write.table(
  uxQuestionnaire[(uxQuestionnaire$GAME_FIRST == 'true'),],
  file = "UX_questionnaire_gameFirst_TRUE.tsv",
  sep = "\t",
  row.names = FALSE,
  col.names = FALSE,
  quote = FALSE
)
write.table(
  uxQuestionnaire[(uxQuestionnaire$GAME_FIRST == 'false'),],
  file = "UX_questionnaire_gameFirst_FALSE.tsv",
  sep = "\t",
  row.names = FALSE,
  col.names = FALSE,
  quote = FALSE
)


# all
dem <- read.csv('DEMOGRAPHICS.csv', head = T)
game <- read.csv('WTP_game.csv', head = T)
quest <- read.csv('WTP_questionnaire.csv', head = T)

demGame <- merge(dem, game, by = 'ID', all.x = T)
demQuest <- merge(dem, quest, by = 'ID', all.x = T)

all <- rbind(demGame, demQuest)
all <- all[order(all$ID), ]

write.csv(all,
          file = "WTP_all.csv",
          row.names = FALSE,
          quote = FALSE)
