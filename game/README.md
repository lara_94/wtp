# Eliciting Willingness to Pay for the EV Charging Service through Gamification

## Introduction ##
This project contains artefacts used for eliciting willingness to pay (WTP) for the EV charging service. There are two ways one can obtain the WTP:

1. through a **questionnaire**; or
2. through a **game**.

## Research Questions ##
1. *Which factors influence person's willingness to pay for the EV charging service?*
2. *How do gamification elements affect the user experience for people subjected to a classic questionnaire as well as a game version of the questionnaire?*