let from;
if (sessionStorage && sessionStorage.getItem('userID')) {
  id = sessionStorage.getItem('userID');
  gameFirst = sessionStorage.getItem('gameFirst');
  from = sessionStorage.getItem('from')
}

if(from === 'game'){
  $('#translation-ux-title-questionnaire').remove();
  $('#translation-ux-description-questionnaire').remove();
} else if (from === 'questionnaire'){
  $('#translation-ux-title-game').remove();
  $('#translation-ux-description-game').remove();
}

const render = results => {
  let tableHtml = '';

  results.forEach((result, index) => {

    tableHtml += '<tr id="row' + result.id + '"><td><div>' +
      '<label>' + result.leftValue + '</label>' +
      '</div></td><div class="radio">' +
      '<td><input type="radio" name="question' + result.id + '" value="1"></td>' +
      '<td><input type="radio" name="question' + result.id + '" value="2"></td>' +
      '<td><input type="radio" name="question' + result.id + '" value="3"></td>' +
      '<td><input type="radio" name="question' + result.id + '" value="4"></td>' +
      '<td><input type="radio" name="question' + result.id + '" value="5"></td>' +
      '<td><input type="radio" name="question' + result.id + '" value="6"></td>' +
      '<td><input type="radio" name="question' + result.id + '" value="7"></td>' +
      '</div><td><div>' +
      '<label>' + result.rightValue + '</label>' +
      '</div></td></tr>';
  });

  $('#ux-questions').html(tableHtml);
};


$.get({
  url: '/ux/questions-' + lang,
  success: render,
});

const finish = () => {
  window.location = '/finish?lang=' + lang;
};

const game = () => {
  window.location = '/game?lang=' + lang;
};

const questionnaire = () => {
  window.location = '/questionnaire?lang=' + lang;
};

$('#experience-submit').on('click touchstart', e => {

  const experience = [];

  $('#ux-questions').removeAttr('style');

  for (let i = 1; i <= 26; i++) {
    let row = '#row' + i;

    if (!$('input[name=question' + i + ']:checked').val()) {
      $(row).css("color", "red");
    } else {
      $(row).css("color", "black");
      let object = {
        id: i,
        answer: $('input[name=question' + i + ']:checked').val()
      };
      experience.push(object);
    }
  }



  if (experience.length === 26) {

    if (gameFirst === 'true' && from === 'game') {

      const url = '/save/ux/game/' + id;
      $.ajax({
        method: 'POST',
        url: url,
        data: JSON.stringify({experience: experience}),
        contentType: 'application/json',
        success: questionnaire()
      });
    } else if (gameFirst === 'true' && from === 'questionnaire') {

      const url = '/save/ux/questionnaire/' + id;
      $.ajax({
        method: 'POST',
        url: url,
        data: JSON.stringify({experience: experience}),
        contentType: 'application/json',
        success: finish()
      });
    } else if (gameFirst === 'false' && from === 'questionnaire') {

      const url = '/save/ux/questionnaire/' + id;
      $.ajax({
        method: 'POST',
        url: url,
        data: JSON.stringify({experience: experience}),
        contentType: 'application/json',
        success: game()
      });
    } else if (gameFirst === 'false' && from === 'game') {

      const url = '/save/ux/game/' + id;
      $.ajax({
        method: 'POST',
        url: url,
        data: JSON.stringify({experience: experience}),
        contentType: 'application/json',
        success: finish()
      });
    }
  } else {
    document.getElementById('experience-submit').style.marginBottom = '0px';
    document.getElementById('translation-ux-warning').style.display = 'block';
  }
});

