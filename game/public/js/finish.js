'use strict';

if (lang === 'hr')
  document.getElementById("code").remove();

const renderTable = results => {
  let tableHtml = '';

  results.forEach((result, index) => {
    tableHtml += '<tr>';

    tableHtml += `<td>${index + 1}.</td><td>${result.user.age}</td><td>${result.user.gender === 'M' ? (lang === 'en' ? 'Male' : 'Muški') : (lang === 'en' ? 'Female' : 'Ženski')}</td><td>${result.timeInGame}s</td>`;

    tableHtml += '</tr>';
  });

  $('#table-body').html(tableHtml);
};

const showTimeModal = result => {
  $('#my-result').html(result);
  $('#user-id').html(userId);
};

$.get({
  url: '/results',
  success: renderTable,
});

let userId;
if (sessionStorage && sessionStorage.getItem('userID')) {
  userId = sessionStorage.getItem('userID');
}

if (userId !== null) {
  $.get({
    url: `/results/${sessionStorage.getItem('userID')}`,
    success: showTimeModal
  });
}

const comment = {text: ''};

$('#comment-submit').on('click touchstart', e => {

  comment.text = $('#comment').val();

  if ($('#comment-submit').hasClass('btn-primary')) {

    $.post('/comment/' + userId, comment, () => {
      $('#comment-submit').html('&#10004;');
      $('#comment-submit').removeClass('btn-primary').addClass('btn-success');
    });
  }
});


sessionStorage.removeItem('userID');
sessionStorage.removeItem('from');
sessionStorage.removeItem('gameFirst');
