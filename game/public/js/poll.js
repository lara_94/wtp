'use strict';

let largeDisplay = true;

if (screenWidth <= 900) {
  largeDisplay = false;
}

let isMobile = false;

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
  isMobile = true;
}

const polls = [];

const timesHR = ['ujutro', 'poslijepodne', 'navečer'];
const timesEN = ['morning', 'afternoon', 'evening'];


let daytime;
let soc1;
let soc2;
let delta;
let cost;

let image;

const pollModal = $('#poll');
const slider = $('#slider');
const batteryRemaining = $('#battery-remaining');
const coverElement = $('#cover');
const pollSubmnitElement = $('#poll-submit');
const firstElement = $('#first');
const secondElement = $('#second');
const thirdElement = $('#third');
const fourthElement = $('#fourth');
const fifthElement = $('#fifth');
const sixthElement = $('#sixth');
const batteryElement = $('#battery');
const batteryTextElement = $('#battery-text');

function newPoll() {

  daytime = timesEN[polls.length % 3];
  soc1 = Math.floor(Math.random() * 100);
  delta = Math.floor(Math.random() * 100);
  soc2 = soc1 + delta > 100 ? 100 : soc1 + delta;
  cost = Math.floor((Math.random() * 100));

  if (daytime === timesEN[0]) {
    image = 'morning';
    if (!largeDisplay || isMobile) image = 'morningMobile';
  }
  if (daytime === timesEN[1]) {
    image = 'afternoon';
    if (!largeDisplay || isMobile) image = 'afternoonMobile';
  }
  if (daytime === timesEN[2]) {
    image = 'night';
    if (!largeDisplay || isMobile) image = 'nightMobile';
  }

  $('#daytime').html(lang === 'en' ? daytime : timesHR[polls.length % 3]);
  $('#soc1').html(soc1);
  $('#delta').html(delta);
  $('#cost').html(cost);

  coverElement.css({'background-image': 'url(../images/' + image + '.png)'});
  pollSubmnitElement.attr('disabled', 'disabled');
  batteryRemaining.css('width', '0');

  pollModal.modal('show');

  firstElement.delay(200).fadeIn();

  // weird way to make async
  setTimeout(() => {
    coverElement.css({'background-image': 'url(../images/' + image + '.png)'}).animate({opacity: 1}, 300);
    secondElement.delay(500).fadeIn();
    thirdElement.delay(1300).fadeIn();

    batteryElement.delay(500).fadeIn(800);

    batteryRemaining.css('background-color', soc1 < 33.5 ? '#f44336' : (soc1 > 66.5 ? '#4caf50' : '#ffeb3b'));
    batteryTextElement.html(`${soc1}%`);

    batteryElement.delay(500).fadeIn(800);

    let width = 1.5 * soc1;

    batteryRemaining.delay(1300).animate({width: width + 'px'}, 800);

    fourthElement.delay(2500).fadeIn(800, () => {
      batteryTextElement.html(`${soc1}%&nbsp;&rarr;&nbsp;${soc2}%`);
    });

    let width2 = 1.5 * soc2;

    batteryRemaining.delay(1000).animate({width: width2 + 'px'}, 800, () => {
      batteryRemaining.css('background-color', soc2 < 33.5 ? '#f44336' : (soc2 > 66.5 ? '#4caf50' : '#ffeb3b'));
    });

    fifthElement.delay(4000).fadeIn();
    sixthElement.delay(5000).fadeIn(800);
  }, 800);

}

const dataSaved = () => {
  sessionStorage.setItem('from', 'game');
  window.location = '/ux?lang=' + lang;
};

pollSubmnitElement.click(e => {
  const wtp = slider.val();
  const poll = {daytime, soc1, soc2, delta, cost, wtp};

  polls.push(poll);

  if (polls.length === 6) {

    timeGame = timeElapsed.getUTCHours() * 3600 + timeElapsed.getUTCMinutes() * 60 + timeElapsed.getUTCSeconds() + timeElapsed.getUTCMilliseconds() / 1000;

    if (sessionStorage && sessionStorage.getItem('userID')) {
      id = sessionStorage.getItem('userID');
    }

    const url = '/save/polls/game/' + id;

    $.ajax({
      method: 'POST',
      url: url,
      data: JSON.stringify({
        polls: polls,
        timeInGame: timeGame,
        gameFirst: sessionStorage.getItem('gameFirst') === "true" ? true : false,
        isMobile
      }),
      contentType: 'application/json',
      success: dataSaved
    });
  }

  e.target.blur();

  slider.val(0);
  sliderValue.html('0');

  firstElement.hide();
  secondElement.hide();
  thirdElement.hide();
  batteryElement.hide();
  fourthElement.hide();
  fifthElement.hide();
  sixthElement.hide();
  pollModal.modal('hide');

  startTimer();
  gameArea.restart();
});

const sliderValue = $('#slider-num b');

const handleSliderChange = e => {
  sliderValue.html(e.target.value);
  pollSubmnitElement.removeAttr('disabled');
};

slider.on('input', handleSliderChange);

if (!largeDisplay) {

  let styles = {
    'width': '100%',
    'margin': '0',
    'padding': '5px 5px 1px 5px'
  };
  $('#poll-content').css(styles);

  styles = {
    'padding': '0',
    'max-width': '360px',
    'max-height': '688px',
    'margin': '0px auto 0 auto'
  };
  $('#poll-modal').css(styles);

  styles = {
    'background-size': '360px'
  };
  $('#cover').css(styles);

  styles = {
    'left': '80px',
    'top': '325px'
  };
  $('#battery').css(styles);

  styles = {
    'left': '0',
    'width': '100%',
    'top': '420px'
  };
  $('#sixth').css(styles);

  styles = {
    'width': '90%',
    'padding': '22px',
    'margin': 'auto'
  };
  $('.slider-container').css(styles);

  styles = {
    'width': '100%'
  };
  $('#slider').css(styles);

  styles = {
    'font-size': 'larger',
    'font-weight': '600',
    'padding': '10px',
  };
  $('#sliderNum').css(styles);

  styles = {
    'font-size': '2rem'
  };
  $('#time').css(styles);
}






















