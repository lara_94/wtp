let url = new URL(window.location.href);
let lang = url.searchParams.get("lang");
if (lang === null) lang = 'en';

const save = results => {
  results.forEach((result, index) => {

    switch (result.key) {
      case 'translation-game-age':
        lang === 'en' ? $('#age').attr("placeholder", result.en) : $('#age').attr("placeholder", result.hr);
        break;
      case 'translation-game-car-cnt':
        lang === 'en' ? $('#car-cnt').attr("placeholder", result.en) : $('#car-cnt').attr("placeholder", result.hr);
        break;
      case 'translation-game-income':
        lang === 'en' ? $('#income').attr("placeholder", result.en) : $('#income').attr("placeholder", result.hr);
        break;
      case 'translation-game-currency':
        lang === 'en' ? $('#basic-addon1').html(result.en) : $('#basic-addon1').html(result.hr);
        break;
      case 'translation-game-btn-start':
        lang === 'en' ? $('#user-submit').html(result.en) : $('#user-submit').html(result.hr);
        break;
      case 'translation-game-help-btn-ok':
        lang === 'en' ? $('#help-submit').html(result.en) : $('#help-submit').html(result.hr);
        break;
      case 'translation-game-q-btn-save':
        lang === 'en' ? $('#poll-submit').html(result.en) : $('#poll-submit').html(result.hr);
        break;
      case 'translation-ux-btn-save':
        lang === 'en' ? $('#experience-submit').html(result.en) : $('#experience-submit').html(result.hr);
        break;
      case 'translation-questionnaire-btn-save':
        lang === 'en' ? $('#questionnaire-submit').html(result.en) : $('#questionnaire-submit').html(result.hr);
        break;
      case 'translation-finish-btn-save':
        lang === 'en' ? $('#comment-submit').html(result.en) : $('#comment-submit').html(result.hr);
        break;
      default:
        let key = '#' + result.key;
        lang === 'en' ? $(key).html(result.en) : $(key).html(result.hr);
    }
  });
};

$.get({
  url: '/translation',
  success: save,
});
