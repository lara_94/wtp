'use strict';

let id;
let timeGame = 0;
let gameFirst;

const userModal = $('#user-modal');
const selectElements = $('select.styled-select');
const alert = $('#validation-alert');

const user = {gender: 'F'};

const handleSexChange = (e, sex) => {
  if (e) {
    user.gender = e.target.value;
  } else {
    user.gender = sex.substr(0, 1);
    $(`#${sex.toLowerCase()}`).prop('checked', true);
  }

  $('#user-submit').removeAttr('disabled');
};

const handleSelectChange = e => {
  if (e.target.value !== '') {
    e.target.style = 'color:black';
  } else {
    e.target.style = '';
  }
};

const validateUser = () => {
  let valid = true;

  $("#income, #age, #car-cnt, #ev-car-owner, #licence, #employment, #car-owner, #familiarity, #country").css('border-color', 'rgba(0, 0, 0, .15)');

  if (user.gender !== 'M' && user.gender !== 'F') {
    valid = false;
  }
  if (isNaN(user.income) || user.income < 0) {
    $('#income').css('border-color', '#a94442');
    valid = false;
  }
  if (isNaN(user.age) || user.age < 10 || user.age > 100) {
    $('#age').css('border-color', '#a94442');
    valid = false;
  }
  if (isNaN(user.carCnt) || user.carCnt < 0 || user.carCnt > 100) {
    $('#car-cnt').css('border-color', '#a94442');
    valid = false;
  }
  if (user.evCarOwner === '') {
    $('#ev-car-owner').css('border-color', '#a94442');
    valid = false;
  }
  if (user.licence === '') {
    $('#licence').css('border-color', '#a94442');
    valid = false;
  }
  if (user.employment === '') {
    $('#employment').css('border-color', '#a94442');
    valid = false;
  }
  if (user.carOwner === '') {
    $('#car-owner').css('border-color', '#a94442');
    valid = false;
  }
  if (user.familiarity === '') {
    $('#familiarity').css('border-color', '#a94442');
    valid = false;
  }

  if (user.country === '') {
    $('#country').css('border-color', '#a94442');
    valid = false;
  }

  return valid;
};

if (sessionStorage.length > 0 && sessionStorage.getItem('gameFirst') === 'false' && sessionStorage.getItem('from') === 'questionnaire') {
  helpModal.modal('show');
  startGame();
} else {
  userModal.modal('show');

}

$('#user-submit').on('click touchstart', e => {

  user.age = parseInt($('#age').val(), 10);
  user.employment = $('#employment').val();
  user.income = parseInt($('#income').val(), 10);
  user.familiarity = $('#familiarity').val();
  user.carCnt = parseInt($('#car-cnt').val(), 10);
  user.evCarOwner = $('#ev-car-owner').val();
  user.licence = $('#licence').val();
  user.carOwner = $('#car-owner').val();
  user.country = $('#country').val();

  if (validateUser()) {
    $.post('/user', user, userId => {
      id = userId;

      if (sessionStorage) {
        sessionStorage.setItem('userID', id);
      }

      if (Math.random() < 0.5) {
        sessionStorage.setItem('gameFirst', false);
        window.location.pathname = `/questionnaire`;

      } else {
        sessionStorage.setItem('gameFirst', true);
        userModal.modal('hide');

        helpModal.modal('show');

        startGame();
      }
    });
  } else {
    alert.attr('style', 'display: block');
    e.target.blur();
  }
});

$('div.sex-container div.sex-wrapper input').change(handleSexChange);

$('.sex-image').on('click', e => {
  handleSexChange(null, e.target.alt);
});

selectElements.change(handleSelectChange);
