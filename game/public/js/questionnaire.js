const submitElement = $('#questionnaire-submit');
const slider = $('#slider');
const sliderValue = $('#slider-num b');
let start = Math.floor((Math.random() * 3) + 1);

const polls = [];

const timesHR = ['ujutro', 'poslijepodne', 'navečer'];
const timesEN = ['morning', 'afternoon', 'evening'];


let daytime;
let soc1;
let soc2;
let delta;
let cost;

let num = 0;

newQPoll();

function newQPoll() {

  num++;

  $('#question-num').html(num);

  daytime = timesEN[polls.length % 3];
  soc1 = Math.floor((Math.random() * 100));
  delta = Math.floor((Math.random() * 100));
  soc2 = soc1 + delta > 100 ? 100 : soc1 + delta;
  cost = Math.floor((Math.random() * 100));

  $('#daytime').html(lang === 'en' ? daytime : timesHR[polls.length % 3]);
  $('#soc1').html(soc1);
  $('#delta').html(delta);
  $('#cost').html(cost);
  $('#soc2').html(soc2);

  submitElement.attr('disabled', 'disabled');

  const handleSliderChange = e => {
    sliderValue.html(e.target.value);
    submitElement.removeAttr('disabled');
  };

  slider.on('input', handleSliderChange);
}

const dataSaved = () => {
  sessionStorage.setItem('from', 'questionnaire')
  window.location = '/ux?lang=' + lang;
};

$(document).keydown(e => {
  const code = e.keyCode ? e.keyCode : e.which;
  var attr = $(submitElement).attr('disabled');

  if(code === 13 && attr !== 'disabled')
    submitElement.click();
});

submitElement.on('click touchstart', e => {
  const wtp = slider.val();
  const poll = {daytime, soc1, soc2, delta, cost, wtp};

  polls.push(poll);

  slider.val(0);
  sliderValue.html(0);

  if (num < 6) {
    newQPoll();

  } else {

    if (sessionStorage && sessionStorage.getItem('userID')) {
      id = sessionStorage.getItem('userID')
    }

    const url = '/save/polls/questionnaire/' + id;

    $.ajax({
      method: 'POST',
      url: url,
      data: JSON.stringify({questionnaire: polls}),
      contentType: 'application/json',
      success: dataSaved
    });
  }
});
